import frappe

def items_to_erpnext_item():
    """From TailPOS items to ERPNext item"""
    items = frappe.get_all('Items')

    # Track the loop
    index = 1
    max_length = len(items)

    for item in items:

        # Get information
        tailpos_item = frappe.get_doc('Items', item.name)

        # Inform user
        print 'Processing {0} - {1}/{2} item.'.format(tailpos_item.description, index, max_length)

        # ERPNext Item
        migrate_item = frappe.get_doc({
            'doctype': 'Item',
            'item_code': tailpos_item.description,
            'item_name': tailpos_item.description,
            'item_group': 'Consumable',
            'stock_uom': 'Unit'
        })

        # Save to the database
        migrate_item.insert()

        # Update index
        index = index + 1

    print 'Finished processing.'

def items_update_erpnext_item():
    """TailPOS update the price"""
    items = frappe.get_all('Items')

    # Track the loop
    index = 1
    max_length = len(items)

    for item in items:

        # Get description
        tailpos_desc = frappe.db.get_value('Items', item.name, 'description')
        tailpos_price = frappe.db.get_value('Items', item.name, 'price')
        tailpos_barcode = frappe.db.get_value('Items', item.name, 'barcode')

        # Inform the user
        print 'Processing {0} - {1}/{2} item'.format(tailpos_desc, index, max_length)

        # Set the Rate and Barcode
        migrate_item = frappe.get_doc('Item', tailpos_desc)
        migrate_item.standard_rate = tailpos_price
        migrate_item.barcode = tailpos_barcode

        # Save the migrated item
        migrate_item.save()

        index = index + 1

    print 'Finished processing'

import frappe


def create_item_prices():
    """Create Item Prices based from the Rate"""
    items = frappe.get_all('Item')

    # Track the lopp
    index = 1
    max_length = len(items)

    for item in items:

        # Get the Standard Rate
        rate = frappe.db.get_value('Item', item.name, 'standard_rate')

        print '[+] Processing {0} - {1}/{2}'.format(item.name, index, max_length)

        # Create an 'Item Price'
        item_price = frappe.get_doc({
            'doctype': 'Item Price',
            'price_list': 'Standard Selling',
            'item_code': item.name,
            'price_list_rate': rate
        })

        item_price.insert()

        index = index + 1

    print 'Finished processing'

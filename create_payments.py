import frappe


def create_payments():
    """Create Payments for the existing Sales Invoice"""
    sales_invoices = frappe.get_all('Sales Invoice')

    for invoice in sales_invoices:

        # Keep the invoice
        invoice = frappe.get_doc('Sales Invoice', invoice.name)

        # If the invoice is already paid, skip
        if invoice.status == 'Paid':
            continue

        # Set the POS profile
        invoice.pos_profile = 'Sample'

        # For payments
        invoice.is_pos = 1

        # Add it to the cash accounts
        invoice.append('payments', {
            'mode_of_payment': 'Cash',
            'amount': invoice.outstanding_amount
        })

        try:
            invoice.save()
            invoice.submit()
            print '[/] Submitted Invoice {0}'.format(invoice.name)
        except Exception as e:
            print str(e)

import frappe


def check_items():
    """Check the items from the receipts that are not listed on the Items"""
    receipts = frappe.get_all('Receipts')

    # Missing items in the database
    missing_items = []

    for receipt in receipts:
        receipt = frappe.get_doc('Receipts', receipt.name)

        for line in receipt.receipt_lines:

            try:
                # Get the item
                item = frappe.get_doc('Item', line.item_name)
            except:
                if line.item_name not in missing_items:
                    missing_items.append(line.item_name)

                    # Create missing items
                    create_item(line)

    print 'There are {0} missing items.'.format(len(missing_items))
    print missing_items


def create_item(item):
    """Create ERPNext Item"""
    print '[/] Created Item {0}'.format(item.item_name)

    new_item = frappe.get_doc({
        'doctype': 'Item',
        'item_code': item.item_name,
        'item_name': item.item_name,
        'item_group': 'Consumable',
        'stock_uom': 'Unit',
        'standard_rate': item.price
    }).insert()
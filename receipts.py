import frappe


def create_sales_invoice_from_receipts():
    """Create the Sales Invoices from the Receipts"""
    from frappe.utils.dateutils import datetime_in_user_format, parse_date

    # Receipts
    receipts = frappe.get_all('Receipts')

    # Track the Loop
    index = 1
    max_length = len(receipts)

    # Error receipts
    error_receipts = []

    # Receipt
    for receipt in receipts:

        receipt = frappe.get_doc('Receipts', receipt.name)

        # Date/Time String
        datetime = datetime_in_user_format(receipt.date).split()

        # Date/Time
        date = parse_date(datetime[0])
        time = datetime[1]

        sales_invoice = frappe.get_doc({
            'doctype': 'Sales Invoice',
            'customer': 'Guest',
            'posting_date': date,
            'posting_time': time,
            'due_date': date
        })

        # Insert lines to each sales invoice
        for line in receipt.receipt_lines:
            sales_invoice.append('items', {
                'item_code': line.item_name,
                'item_name': line.item_name,
                'uom': 'Unit',
                'qty': line.qty,
                'rate': line.price
            })

        try:
            # Create the Sales Invoice
            sales_invoice.insert()
        except Exception as e:
            error_receipts.append(receipt.receiptnumber)
            print str(e)

    print 'There are {0} errors encountered.'.format(len(error_receipts))
    print 'The following are the receipts with the error:'
    print error_receipts
